+++
date = "2019-01-04T01:00:00-07:00"
title = "Acervo de Dados"
url = "/acervo"
+++

O que já foi salvo:

* [Sites obtidos pelo Archive Team durante a operação "2018 Brazilian Elections"](https://www.archiveteam.org/index.php?title=ArchiveBot/2018_Brazilian_general_elections).
