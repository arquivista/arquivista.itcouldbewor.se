+++
date = "2019-01-04T01:00:00-07:00"
title = "Desserviços ao conhecimento"
url = "/desservicos"
+++

> "Era um prazer especial ver as coisas serem devoradas, ver as coisas serem
  enegrecidas e alteradas. Empunhando o bocal de bronze, a grande víbora cuspindo seu
  querosene peçonhento sobre o mundo, o sangue latejava em sua cabeça e suas mãos eram
  as de um prodigioso maestro regendo todas as sinfonias de chamas e labaredas para
  derrubar os farrapos e as ruínas carbonizadas da história. Na cabeça impassível, o
  capacete simbólico com o número 451 e, nos olhos, a chama laranja antecipando o que
  viria a seguir, ele acionou o acendedor e a casa saltou numa fogueira faminta que
  manchou de vermelho, amarelo e negro o céu do crepúsculo. A passos largos ele
  avançou em meio a um enxame de vaga-lumes. Como na velha brincadeira, o que ele
  mais desejava era levar à fornalha um marshmallow na ponta de uma vareta, enquanto os
  livros morriam num estertor de pombos na varanda e no gramado da casa. Enquanto os
  livros se consumiam em redemoinhos de fagulhas e se dissolviam no vento escurecido
  pela fuligem.

> Montag abriu o sorriso feroz de todos os homens chamuscados e repelidos pelas
  chamas.

> Sabia que quando regressasse ao quartel dos bombeiros faria vista grossa a si mesmo
  no espelho, um menestrel de cara pintada com rolha queimada. Depois, ao ir para a
  cama, sentiria no escuro o sorriso inflamado ainda preso aos músculos da face. Nunca
  desaparecia, aquele sorriso, nunca, até onde conseguia se lembrar."

> -- Ray Bradbury, Fahrenheit 451

Conteúdos de interesse público removidos da internet e veículos tesourados:

* 13/07/2024 - [Ministério do Meio Ambiente de Bolsonaro apagou 30 anos de documentos](https://www.metropoles.com/colunas/guilherme-amado/ministerio-do-meio-ambiente-de-bolsonaro-apagou-30-anos-de-documentos)
* 03/03/2021 - [Cobrança por acesso automatizado a dados públicos é uma ameaça à inovação cívica e à democracia](https://www.ok.org.br/noticia/cobranca-por-acesso-automatizado-a-dados-publicos-e-uma-ameaca-a-inovacao-civica-e-a-democracia-veto-ja/)
* 13/02/2021 - [Governo reduz transparência e participação social na área ambiental, mostra estudo](https://www.socioambiental.org/pt-br/noticias-socioambientais/governo-reduz-transparencia-e-participacao-social-na-area-ambiental-mostra-estudo?mc_cid=550f96e3d9&mc_eid=f0677180df)
* 20/11/2020 - [Em novo site, Ministério do Meio Ambiente suprime dados sobre áreas protegidas brasileiras ](https://conexaoplaneta.com.br/blog/em-novo-site-ministerio-do-meio-ambiente-suprime-dados-sobre-areas-protegidas-brasileiras/)
* 06/06/2020 - [Após reduzir boletim diário, governo Bolsonaro retira dados acumulados da Covid-19 do site](https://g1.globo.com/politica/noticia/2020/06/06/apos-reduzir-boletim-governo-bolsonaro-retira-dados-acumulados-da-covid-19-de-site-oficial.ghtml)
* 07/04/2020 - [Exército tira do ar estudo que defende forma de isolamento que contraria Bolsonaro ](https://www1.folha.uol.com.br/poder/2020/04/exercito-tira-do-ar-estudo-que-defende-forma-de-isolamento-que-contraria-bolsonaro.shtml)
* 25/03/2020 - [MP de Bolsonaro permite ao governo ignorar até 4 mil pedidos de informação](https://noticias.uol.com.br/politica/ultimas-noticias/2020/03/25/coronavirus-mp-928-governo-ignorar-milhares-pedidos-de-informacao-lai.htm)
* 24/03/2020 - [Só venceremos a pandemia com transparência](https://br.okfn.org/2020/03/24/so-venceremos-a-pandemia-com-transparencia/)
* 10/03/2020 - [Extinção do Comitê de Dados Abertos da Fundação Cultural Palmares](http://www.in.gov.br/en/web/dou/-/portaria-n-45-de-2-de-marco-de-2020-247018684)
* 31/01/2020 - [Governo Bolsonaro não explica tamanho real da fila do Bolsa Família](https://brasil.elpais.com/brasil/2020-01-31/governo-bolsonaro-nao-explica-tamanho-real-da-fila-do-bolsa-familia.html)
* 31/01/2020 - [OEA promoverá audiência sobre violações à liberdade de expressão no Brasil](http://baraodeitarare.org.br/site/noticias/sobre-o-barao/oea-promovera-audiencia-tematica-sobre-violacoes-a-liberdade-de-expressao-no-brasil)
* 29/10/2019 - [Brasil é o 9º país do mundo em impunidade contra jornalistas](http://portalimprensa.com.br/noticias/ultimas_noticias/82805/brasil+e+o+9+pais+do+mundo+em+impunidade+contra+jornalistas)
* 28/08/2019 - [GOVERNO BOLSONARO DECRETA SIGILO SOBRE VISITAS NO PALÁCIO DA ALVORADA](https://epoca.globo.com/guilherme-amado/governo-bolsonaro-decreta-sigilo-sobre-visitas-no-palacio-da-alvorada-23909255)
* 26/04/2019 - [Ministério tira do ar mapa e informações de áreas de conservação de biomas](https://noticias.uol.com.br/meio-ambiente/ultimas-noticias/redacao/2019/04/26/ministerio-tira-do-ar-mapa-e-informacoes-de-areas-de-conservacao-de-biomas.htm)
* 24/01/2019 - [Governo permite que servidores comissionados imponham sigilo ultrassecreto a dados públicos](https://g1.globo.com/politica/noticia/2019/01/24/governo-delega-competencia-para-decretar-sigilo-de-informacoes-publicas.ghtml)
* 23/10/2019 - [Ricardo Salles bloqueia Greenpeace no Twitter](https://www.poder360.com.br/brasil/ricardo-salles-bloqueia-greenpeace-no-twitter/)
* 19/02/2019 - [APÓS INCONSISTÊNCIA NOS DADOS DO MAIS MÉDICOS, SAÚDE TIRA PÁGINA DO AR](https://epoca.globo.com/guilherme-amado/apos-inconsistencia-nos-dados-do-mais-medicos-saude-tira-pagina-do-ar-23462505)
* 16/12/2019 - [94% dos jornalistas têm problemas para obter dados públicos via Lei de Acesso](https://www.poder360.com.br/midia/94-dos-jornalistas-tem-problemas-para-obter-dados-publicos-via-lei-de-acesso/)
* 14/03/2019 - [Governo ordena que Ibama não responda mais pedidos da imprensa](https://exame.abril.com.br/brasil/governo-ordena-que-ibama-nao-responda-mais-pedidos-da-imprensa/)
* 13/03/2019 - [Damares arrola Olavo de Carvalho e chanceler como testemunhas ao processar repórter](https://www.jota.info/paywall?redirect_to=//www.jota.info/coberturas-especiais/liberdade-de-expressao/damares-alves-processa-jornalistas-13032019)
* 11/05/2018 - [20 projetos de lei no Congresso pretendem criminalizar fake news](https://apublica.org/2018/05/20-projetos-de-lei-no-congresso-pretendem-criminalizar-fake-news/)
* 11/04/2019 - [Decreto publicado em diário oficial da União: DECRETO Nº 9.756, DE 11 DE ABRIL DE 2019 Institui o portal único "gov.br" e dispõe sobre as regras de unificação dos canais digitais do Governo federal.](http://www.in.gov.br/materia/-/asset_publisher/Kujrw0TZC2Mb/content/id/71137353/do1e-2019-04-11-decreto-n-9-756-de-11-de-abril-de-2019-71137307)
* 08/04/2019 - [TRÊS ALERTAS: BOLSONARO VEM DIFICULTANDO ACESSO À INFORMAÇÃO PÚBLICA](https://epoca.globo.com/tres-alertas-bolsonaro-vem-dificultando-acesso-informacao-publica-23582446)
* 07/2019 - [Dados abertos e meio ambiente: uma avaliação dos planos de dados abertos dos órgãos federais ambientais do Brasil](http://www.imaflora.org/downloads/biblioteca/5d35dfbc9118a_Dados_abertos_e_meio_ambiente.pdf)
* 05/12/2019 - [Sob Salles, ministério deixa 8 em 10 jornalistas sem resposta](https://www.oeco.org.br/reportagens/sob-salles-ministerio-deixa-8-em-10-jornalistas-sem-resposta/)
* 05/06/2019 - [Sancionada lei que criminaliza denunciação caluniosa contra candidato em eleição](https://www.conjur.com.br/2019-jun-05/sancionada-lei-criminaliza-calunia-candidato-cargo-politico)
* 04/02/2020 - [PSOL vê crime de responsabilidade nas respostas do MMA sobre desmatamento](https://www.jb.com.br/pais/ecologia/2020/02/1022026-psol-ve-crime-de-responsabilidade-nas-respostas-do-mma-sobre-desmatamento.html)
* 04/02/2019 - [O ministro do Meio Ambiente bloqueou ONGs do setor em sua conta oficial no Twitter](https://www.osul.com.br/o-ministro-do-meio-ambiente-bloqueou-ongs-do-setor-em-sua-conta-oficial-no-twitter/)
* 04/01/2019 - [Ministério da Saúde retira do ar cartilha voltada para saúde do homem trans](https://www1.folha.uol.com.br/cotidiano/2019/01/ministerio-da-saude-retira-do-ar-cartilha-voltada-para-saude-do-homens-trans.shtml)
* 03/02/2020 - [Gestão Doria desmantela acervos e bases cartográficas de planejamento urbano](http://www.diretodaciencia.com/2020/02/03/gestao-doria-desmantela-acervos-e-bases-cartograficas-de-planejamento-urbano/)
* 07/08/2019 - [Busca no Cadastro Nacional de Unidades de Conservação está inacessível](http://www.diretodaciencia.com/2019/08/07/busca-no-cadastro-nacional-de-unidades-de-conservacao-esta-inacessivel/).
* 02/08/2019 - [Diretor do Inpe é demitido após desafiar Bolsonaro](https://congressoemfoco.uol.com.br/meio-ambiente/diretor-do-inpe-e-demitido-apos-desafiar-bolsonaro/)
* 05/08/2019 - [‘Apagão’ de dados é risco para toda a sociedade: queremos mais transparência e respeito à ciência](https://br.okfn.org/2019/08/05/apagao-de-dados-e-risco-para-toda-a-sociedade-queremos-mais-transparencia-e-respeito-a-ciencia/).
* 25/04/2019 - [Gestão Salles exclui da internet dados de áreas prioritárias para conservação](http://www.diretodaciencia.com/2019/04/25/gestao-salles-exclui-da-internet-dados-de-areas-prioritarias-para-conservacao/) ([matéria alternativa](https://noticias.uol.com.br/meio-ambiente/ultimas-noticias/redacao/2019/04/26/ministerio-tira-do-ar-mapa-e-informacoes-de-areas-de-conservacao-de-biomas.htm)).
* 08/04/2019 - [Três alertas: Bolsonaro vem dificultando acesso à informação pública](https://epoca.globo.com/tres-alertas-bolsonaro-vem-dificultando-acesso-informacao-publica-23582446).
* 04/02/2019 - [O ministro do arremedo - Como Ricardo Salles adulterou um mapa ambiental para beneficiar mineradoras](https://theintercept.com/2019/02/03/ricardo-salles-mineradoras/)
* 30/01/2019 - [TV pública para surdos exclui vídeos sobre esquerda e filósofos](https://www1.folha.uol.com.br/poder/2019/01/tv-publica-para-surdos-exclui-videos-sobre-esquerda-e-filosofos.shtml)
* 08/01/2019 - [Após medida de Temer, rádios comunitárias poderão ser alvo de Bolsonaro](https://www.redebrasilatual.com.br/cidadania/2019/01/apos-medida-de-temer-radios-comunitarias-poderao-ser-alvos-de-bolsonaro)
* 04/01/2019 - [Ministério da Saúde retira do ar cartilha voltada para saúde do homem trans](https://www1.folha.uol.com.br/cotidiano/2019/01/ministerio-da-saude-retira-do-ar-cartilha-voltada-para-saude-do-homens-trans.shtml)
