+++
date = "2019-01-04T01:00:00-07:00"
title = "Referências"
url = "/referencias"
+++

## Documentários

* [Digital Amnesia | Full Documentary](https://www.youtube.com/watch?v=krg3lrxHX2k).

## Guias e documentações

* [Manual de defesa contra a censura nas escolas](http://www.manualdedefesadasescolas.org/manualdedefesa.pdf) ([sobre](http://www.cartaeducacao.com.br/reportagens/manual-orienta-professores-e-escolas-a-como-se-defenderem-de-atos-de-censura/))
* [Personal Digital Archiving | Digital Preservation - Library of Congress](http://www.digitalpreservation.gov/personalarchiving/index.html)
* [Websites, Blogs, Social Media - Personal Archiving | Digital Preservation - Library of Congress](http://www.digitalpreservation.gov/personalarchiving/websites.html)
* [Web ARChive - Wikipedia](https://en.wikipedia.org/wiki/Web_ARChive)
* [Archiving web sites - LWN](https://lwn.net/Articles/766374/)
* [Archiving web sites - anarcat blog](https://anarc.at/blog/2018-10-04-archiving-web-sites/)
* [Restoring - Archive Team Wiki](https://www.archiveteam.org/index.php?title=Restoring)

## Projetos, Comunidades e Organizações

* [The Eye - non-profit website dedicated towards content archival and long-term preservation](https://github.com/The-Eye-Team/) ([site](https://the-eye.eu/))
* [Web Archiving Community](https://github.com/pirate/ArchiveBox/wiki/Web-Archiving-Community).
* [Queima de arquivo não!](https://queimadearquivonao.webnode.com/) ([PL 7920/2017 - Digitalização de Documentos](http://www.camara.gov.br/proposicoesWeb/fichadetramitacao?idProposicao=2142105))
* [Climate change mirror](http://climatemirror.org/) ([dados salvos](https://climate.daknob.net/) / [como ajudar](https://github.com/climate-mirror/how-to-help))
* [r/DataHoarder](https://www.reddit.com/r/DataHoarder/)
* [Archive Team](http://archiveteam.org) / [ArchiveBot](https://www.archiveteam.org/index.php/ArchiveBot).
* [IIPC - netpreserve.org](http://netpreserve.org/projects/)
* [Community Owned digital Preservation Tool Registry (COPTR)](http://coptr.digipres.org/Main_Page)
* [DigiPres Commons Community-owned digital preservation resources](http://digipres.org)
* [PrestoSpace](http://www.prestospace.org/project/public.en.html)
* [Emulation-As-A-Service](http://bw-fla.uni-freiburg.de/)
* [Brasil.IO](https://brasil.io)
* [AUT Project](http://archivesunleashed.org/)
* [Webarchive UNESCO](http://webarchive.unesco.org/)
* [Projeto Brasil Nunca Mais - hospedado no Ministério Público Federal](http://bnmdigital.mpf.mp.br/pt-br/)
* [The End of Term Web Archive](http://eotarchive.cdlib.org).
* [NetarchivesSuite](https://sbforge.org/display/NAS) ([código](https://sbforge.org/display/NAS))
* [Common Crawl](https://commoncrawl.org/)
* [International Internet Preservation Consortium](http://netpreserve.org/)
* [List of Web archiving initiatives](https://en.wikipedia.org/wiki/List_of_Web_archiving_initiatives)

## Softwares

* [Wget](https://www.gnu.org/software/wget/) [com dica para baixar mais rápido](https://stackoverflow.com/questions/3430810/wget-download-with-multiple-simultaneous-connections)
* [Wget2](https://gitlab.com/gnuwget/wget2)
* [httrack](https://www.httrack.com/), [httraqt](http://httraqt.sourceforge.net/) (httrack GUI)), [webhttrack](https://packages.debian.org/stretch/webhttrack) e [httrack-android](https://github.com/xroche/httrack-android)
* [crawl](https://git.autistici.org/ale/crawl/)
* [aria2](https://aria2.github.io/) [pacote](https://packages.debian.org/stable/aria2)
* [wpull](https://github.com/ArchiveTeam/wpull)
* [grab-site](https://github.com/ludios/grab-site)
* [ckanext-harvest](https://github.com/ckan/ckanext-harvest/)
* [Web archiving using Google Chrome](https://github.com/PromyLOPh/crocoite)
* [IIPC - Recurso geral sobre arquivo web](https://github.com/iipc/awesome-web-archiving)
* [Web Arquivos públicos](https://github.com/webrecorder/public-web-archives)
* [Datatogether Research](https://github.com/datatogether/research/)
* [Comparacao entre os softwares](https://github.com/datatogether/research/tree/master/web_archiving)
* [Crawler do Internet Archive](https://github.com/internetarchive/brozzler)
* [WebArchiver](https://github.com/ArchiveTeam/WebArchiver)
* [Warrick](https://code.google.com/archive/p/warrick/): free utility for reconstructing (or recovering) a website when a back-up is not available. Warrick utilizes the Memento Framework (http://www.mementoweb.org) to discover archived versions of resources from web archives. The resources are gathered to provide a single collection of files.
* [Perma.cc - a service that helps prevent link rot](https://perma.cc): serviço pago mas que possui código aberto
* [Wayback Machine Downloader](https://github.com/hartator/wayback-machine-downloader): download an entire website from the Wayback Machine.
* [OpenArchive](https://open-archive.org/save):
    * [OpenArchive · GitHub](https://github.com/OpenArchive)
    * [OpenArchive/Save-app-android: This is the Save app for Android](https://github.com/OpenArchive/Save-app-android)
* WARC:
    * [The WARC Ecosystem](https://www.archiveteam.org/index.php?title=The_WARC_Ecosystem)
    * [WarcProxy](https://github.com/odie5533/WarcProxy)
    * [twarc](https://github.com/docnow/twarc)
    * [warcprox - WARC writing MITM HTTP/S proxy](https://github.com/internetarchive/warcprox)
    * [warctozip](https://github.com/alard/warctozip)
    * [warctools](https://github.com/internetarchive/warctools)
    * [httrack2warc: Converts HTTrack crawls to WARC files](https://github.com/nla/httrack2warc)
    * [WARC diff tools](https://github.com/harvard-lil/WARC-diff-tools)
* Indexing / full text search:
    * [Webarchive Discovery](https://github.com/ukwa/webarchive-discovery): WARC and ARC indexing and discovery tools: provides full-text search for our web archives.
    * [SolrWayBack](https://github.com/netarchivesuite/solrwayback): A search interface and wayback machine for the UKWA Solr based warc-indexer framework.
    * [shine](https://github.com/netarchivesuite/shine)
    * [warclight](https://github.com/archivesunleashed/warclight)
    * [lockss-solr](https://github.com/edina/lockss-solr)
* Assinatura de acervos:
    * [notary](https://github.com/theupdateframework/notary)
    * [tlsnotary](https://tlsnotary.org)

## Especificações

* [RFC7089 - HTTP Framework for Time-Based Access to Resource States -- Memento](https://tools.ietf.org/html/rfc7089)
* [RFC5854 - The Metalink Download Description Format](https://tools.ietf.org/html/rfc5854).
* [Format Description Categories - Sustainability of Digital Formats | Library of Congress](https://www.loc.gov/preservation/digital/formats/fdd/descriptions.shtml)
* [The WARC Format 1.1](https://iipc.github.io/warc-specifications/specifications/warc-format/warc-1.1/)
* [ARC_IA, Internet Archive ARC file format](https://www.loc.gov/preservation/digital/formats/fdd/fdd000235.shtml)
* [WARC, Web ARChive file format](https://www.loc.gov/preservation/digital/formats/fdd/fdd000236.shtml)
* [IIPC Framework Working Group - The WARC File Format (Version 0.9)](http://archive-access.sourceforge.net/warc/warc_file_format-0.9.html)
* [Draft: Information and Documentation - The WARC file format - v1](http://bibnum.bnf.fr/WARC/WARC_ISO_28500_version1_latestdraft.pdf)
* [Format Descriptions for Archived Web Sites and Pages](https://www.loc.gov/preservation/digital/formats/fdd/webarch_fdd.shtml)
* [Wayback Machine API](https://archive.org/help/wayback_api.php)
* [FAIR data](https://www.go-fair.org/) ([princípios](https://www.go-fair.org/fair-principles/), [artigo na Wikipedia](https://en.wikipedia.org/wiki/FAIR_data))
* [The Open Definition](https://opendefinition.org/)
