+++
date = "2019-01-05T01:00:00-07:00"
title = "A Grande Simplificação"
url = "/simplificacao"
+++

> "Foi assim que, após o Dilúvio, a Precipitação Radiativa, as pragas, a
> loucura, a confusão de línguas, a ira, começou, a carnificina da
> Simplificação, quando os remanescentes da humanidade esquartejaram outros
> remanescentes, membro a membro, matando governantes, cientistas, líderes,
> técnicos, professores de todas e quaisquer pessoas que os líderes das turbas
> ensandecidas dissessem que mereciam morrer por terem ajudado a tornar a Terra
> aquilo que ela havia se tornado. Nada tinha sido mais odioso aos integrantes
> das turbas do que os homens instruídos; no princípio, porque haviam servidor
> aos príncipes, mas depois porque se recusavam a participar das carnificinas e
> tentavam se opor às turbas, chamando aquela multidão de 'corja de simplórios
> com sede de sangue'.

> A turba efervescente aceitos o apelido e assumiu o brado. 'Simplórios! Sim,
> sim! Sou simplório! Você é simplório? Vamos construir uma cidade e chamar de
> Cidade dos Simplórios porque até lá todos vocês, seus sofisticados
> desgraçados que provocaram tudo isso, vocês estarão mortos! Simplórios! Em
> frente! Isso vai mostrar a eles! Alguém aqui não é simplório? Atrás do
> maldito, se não for!'"

> -- Walter M. Miller Jr., Um Cântico Para Leibowitz, página 83

[Incêndios](https://pt.wikipedia.org/wiki/Queima_de_livros_na_Alemanha_Nazista), ameaças a conteúdos e à diversidade:

* 15/03/2023 - [Governo Bolsonaro incinerou medicamentos de alto custo](https://www1.folha.uol.com.br/equilibrioesaude/2023/03/governo-bolsonaro-incinerou-medicamentos-de-alto-custo-para-doencas-raras.shtml)
* 12/11/2022 - [Arquivos dos computadores do Planalto são apagados por questões de 'segurança'](https://www.jb.com.br/pais/politica/2022/11/1040678-arquivos-dos-computadores-do-planalto-sao-apagados-por-questoes-de-seguranca.html)
* 11/11/2022 - [Queima de arquivo? Dados de computadores do Planalto são apagados após derrota de Bolsonaro](https://jc.ne10.uol.com.br/colunas/jamildo/2022/11/15119134-queima-de-arquivo-dados-de-computadores-do-planalto-sao-apagados-apos-derrota-de-bolsonaro.html)
* 11/11/2022 - [Computadores do Planalto são apagados | Revista Fórum](https://revistaforum.com.br/politica/2022/11/11/computadores-do-planalto-so-apagados-127247.html)
* 04/03/2022 - [Servidores denunciam o descarte de documentos sobre a ditadura - CartaCapital](https://www.cartacapital.com.br/politica/servidores-denunciam-o-descarte-de-documentos-sobre-a-ditadura/)
* 07/08/2019 - [Estudo aponta que principais órgãos ambientais do governo não cumprem na íntegra legislação de transparência de dados](https://www.ecodebate.com.br/2019/08/07/estudo-aponta-que-principais-orgaos-ambientais-do-governo-nao-cumprem-na-integra-legislacao-de-transparencia-de-dados/) ([download](http://www.imaflora.org/download-form.php?id=949))
* 02/08/2019 - [Diretor do Inpe será exonerado após críticas do governo a dados de desmate](https://www1.folha.uol.com.br/ambiente/2019/08/diretor-do-inpe-sera-exonerado-apos-criticas-do-governo-a-dados-de-desmate.shtml).
* 22/07/2019 - [Bolsonaro diz que divulgação de dados ambientais do Inpe 'dificulta' negociações comerciais](https://g1.globo.com/natureza/noticia/2019/07/22/bolsonaro-diz-que-divulgacao-de-dados-do-inpe-sobre-desmatamento-dificulta-negociacoes-comerciais.ghtml).
* 19/07/2019 - ['Se não puder ter filtro, nós extinguiremos a Ancine', diz Bolsonaro](https://g1.globo.com/politica/noticia/2019/07/19/se-nao-puder-ter-filtro-nos-extinguiremos-a-ancine-diz-bolsonaro.ghtml).
* 04/06/2019 - [Órgãos federais negam acesso a 323 documentos considerados públicos](https://noticias.uol.com.br/politica/ultimas-noticias/2019/06/04/governo-sigilo-documentos-liberados-publico-transparencia.htm).
* 29/05/2019 - [Estudo da Fiocruz sobre uso de drogas no Brasil é censurado](https://g1.globo.com/rj/rio-de-janeiro/noticia/2019/05/29/estudo-da-fiocruz-sobre-uso-de-drogas-no-brasil-e-censurado.ghtml).
* 20/04/2019 - Extinção da [Lista sobre Extinção](http://www.icmbio.gov.br/portal/faunabrasileira/lista-de-especies): [Ministério da Agricultura pede fim da lista de animais aquáticos ameaçados](https://www1.folha.uol.com.br/ambiente/2019/04/ministerio-da-agricultura-pede-fim-da-lista-de-animais-aquaticos-ameacados.shtml).
* 11/04/2019 - Na migração para o [portal único gov.br](http://www.planalto.gov.br/ccivil_03/_ato2019-2022/2019/Decreto/D9756.htm), muito conteúdo pode desaparecer.
* 24/04/2019 - [Como fica o patrimônio histórico com as mudanças na Lei de Incentivo à Cultura](https://oglobo.globo.com/cultura/como-fica-patrimonio-historico-com-as-mudancas-na-lei-de-incentivo-cultura-23619217?).
* 05/04/2019 - [Censura: o fantasma que volta a assustar as bibliotecas brasileiras](http://biblioo.info/censura-o-fantasma-que-volta-a-assustar-as-bibliotecas-brasileiras/).
* 10/01/2019 - [Governo de SP eliminou 2.644 toneladas de documentos sem valor histórico](https://www1.folha.uol.com.br/colunas/monicabergamo/2019/01/governo-de-sp-eliminou-2644-toneladas-de-documentos-sem-valor-historico.shtml)
* 11/12/2018 - [Incêndio destrói imóvel de madeira da Secretaria de Meio Ambiente, em Campinas](https://g1.globo.com/sp/campinas-regiao/noticia/2018/12/11/incendio-destroi-casa-em-campinas-e-ninguem-fica-ferido.ghtml)
* 23/10/2018 - [Bolsonaro é o político que mais tenta censurar publicações na internet](https://theintercept.com/2018/10/23/bolsonaro-censura/)
* 04/10/2018 - [Obras sobre direitos humanos são danificadas na biblioteca da UnB](https://www.correiobraziliense.com.br/app/noticia/cidades/2018/10/04/interna_cidadesdf,710276/obras-sobre-direitos-humanos-sao-danificadas-na-biblioteca-da-unb.shtml).
* 28/09/2018 - [General ligado a Bolsonaro fala em banir livros sem "a verdade" sobre 1964](https://noticias.uol.com.br/politica/eleicoes/2018/noticias/2018/09/28/general-ligado-a-bolsonaro-fala-em-banir-livros-sem-a-verdade-sobre-1964.htm)
* 02/09/2018 - [Incêndio no Museu Nacional do Brasil](https://pt.wikipedia.org/wiki/Inc%C3%AAndio_no_Museu_Nacional_do_Brasil_em_2018)
* 21/02/2018 - [Fiocruz é alvo de censura em pesquisa sobre agrotóxicos](http://www.mst.org.br/2018/02/21/fiocruz-e-alvo-de-censura-em-pesquisa-sobre-agrotoxicos.html)
* 13/05/1891 - [Queima dos arquivos da escravidão no Brasil](https://pt.wikipedia.org/wiki/Queima_dos_arquivos_da_escravid%C3%A3o_no_Brasil)
